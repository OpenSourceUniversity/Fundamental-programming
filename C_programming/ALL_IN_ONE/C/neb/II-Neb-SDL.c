/*code test
Sélectionnez
*/

1.cpp
======


#include "SDL/SDL.h"
 
int main( int argc, char* args[] ) { 
	//Demarrer SDL 
	SDL_Init( SDL_INIT_EVERYTHING ); 
 
	//Quitter SDL 
	SDL_Quit(); 
 
	return 0; 
}

/*

$ g++ -o myprogram mysource.cpp -lSDLmain -lSDL

*/






/*
==============================================================
 programme sauvé : extrait de main.cpp   issu de la creation d'un projet sdl avec  codeblock
  
*/

/*


 # include <stdlib.h>
 # include <stdio.h>
# include <SDL/SDL.h>

int main (int argc , char * argv [])
 {
SDL_Init ( SDL_INIT_VIDEO );

//fenêtre de taille 640 x 480 en 32 bits / pixel (milliards de couleurs) qui sera chargée en mémoire vidéo

SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

 SDL_Quit ();

return EXIT_SUCCESS ;
}*/


/*
$ gcc main.c `sdl-config --cflags --libs` ; ./a.out


*/

/*
==============================================================
 
*/

 # include <stdlib.h>
# include <stdio.h>
 # include <SDL/SDL.h>

 void pause ();

 int main (int argc , char * argv [])
{
 SDL_Init ( SDL_INIT_VIDEO ); // Initialisation de la SDL

 SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE ); // Ouverture   de la fenêtre
 
 
 pause (); // Mise en pause du programme

 SDL_Quit (); // Arrêt de la SDL

 return EXIT_SUCCESS ; // Fermeture du programme
 }

 void pause ()
 {
int continuer = 1;
 SDL_Event event ;

 while ( continuer )
 {
 SDL_WaitEvent (& event );
 switch ( event . type )
{
 case SDL_QUIT :
continuer = 0;
 }
}
 }


/*
$ gcc main.c `sdl-config --cflags --libs` ; ./a.out


t  OK: parfait, la fenêtre ouvre




*/

==============================================================
 
 
/* ----------------------------------------------------- */





/*




#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>



 int main ( int argc , char * argv []) {


 if ( SDL_Init ( SDL_INIT_VIDEO ) == -1)
{
 fprintf ( stderr , " Erreur d' initialisation de la SDL");
exit ( EXIT_FAILURE );
}

SDL_Quit ();

 return EXIT_SUCCESS ;
 } */



/*
Exemple de compilation en C/SDL


gcc -o executable fichier1.c  fichier3.c ...  `sdl-config --cflags --libs`




> 
gcc -o exe  1.c    `sdl-config --cflags --libs`




$ gcc -o exe  1.c    `sdl-config --cflags --libs`
imhotep@imhotep-Aspire-5733Z:


*/



 # include <stdlib.h>
 # include <stdio.h>
 # include <SDL/SDL.h>

 //  #include <SDL2/SDL.h>   for SDL2 

int main (int argc , char * argv [])
 {
SDL_Init ( SDL_INIT_VIDEO );

SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

 SDL_Quit ();

return EXIT_SUCCESS ;
}

/*5. J'ai volontairement retiré la gestion d'erreur pour rendre le code plus lisible et plus court, mais
vous devriez dans un vrai programme prendre toutes les précautions nécessaires et gérer les erreurs.



------------------------------------------

 $ g++  1.cpp  -w -lSDL2 -o     exe   ;



----------------------------------------


gcc mon_fichier.c `sdl-config --cflags --libs`

-----------------------------------

>  gcc 1.cpp `sdl-config --cflags --libs`


./a.out


OK
*/





############################################################
############################################################

======
/*
créer un dégradé
on veut créer un dégradé vertical allant du noir au blanc

*/





#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <math.h>       // l1   :  avec ou sans 


void pause ();

 int main (int argc , char * argv []){
//créer notre tableau de 256 SDL_Surface*. On l'initialise à NULL :

//On crée aussi une variable i dont on aura besoin pour nos for.
SDL_Surface * ecran = NULL , * lignes [256] = { NULL };
SDL_Rect position ;
int i = 0;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640,256,32, SDL_HWSURFACE );

for(i = 0 ; i <= 255 ; i++)

//insertion des couleurs :  de ,640, 1, 0, 0, 0, 0

lignes[i] = SDL_CreateRGBSurface (SDL_HWSURFACE,640, 1, 32, 0, 0, 0, 0);


//
SDL_WM_SetCaption("Mon dégradé en SDL !", NULL );

 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 0, 0, 0));

for (i = 0 ; i <= 255 ; i++){
position.x = 0; // Les lignes sont à gauche ( abscisse de 0)
position.y = i; // La position verticale dé pend du numéro de la ligne


SDL_FillRect(lignes [i], NULL , SDL_MapRGB (ecran ->format ,i, i, i));


SDL_BlitSurface(lignes[i], NULL , ecran , &position );
}

SDL_Flip(ecran);

pause();


for(i = 0 ; i <= 255 ; i++) // N'oubliez pas de lib érerles 256 surfaces

SDL_FreeSurface ( lignes [i]);

SDL_Quit ();

return EXIT_SUCCESS;

}



/*
gcc -o  exe   01L01.c   -lSDL

ok


*/
 
 

############################################################
############################################################

/*

LA GESTION DES ÉVÉNEMENTS

*/


#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>



int main (int argc , char * argv [])
{
SDL_Surface * ecran = NULL ;
SDL_Event event ; /* La variable contenant l'évé nement */
int continuer = 1; /* Notre bool éen pour la boucle */

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des évé nements en SDL", NULL );

while ( continuer ) /* TANT QUE la variable ne vaut pas 0 */
{
SDL_WaitEvent (& event ); /* On attend un évé nement qu'on
récupère dans event */
 switch ( event.type ) /* On teste le type d'évé nement */
{
case SDL_QUIT : /* Si c'est un évé nement QUITTER */
continuer = 0; /* On met le bool éen à 0, donc la boucle va s'arr êter */
break ;
}
}

 SDL_Quit ();

 return EXIT_SUCCESS ;
 }



############################################################
############################################################
/*342
CHARGER UNE IMAGE BMP

*/



 
 /*
 
 Ici il n'y a pas d'icone de lancement integré au programme. 
 
 Remarquer  qu'à l'execution, au niveau  de la barre de tache, le fond d icone est vide. 
 
 */
 
 //----------------------------------====
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

void pause();

int main ( int argc , char * argv [])
 {
 SDL_Surface * ecran = NULL , * imageDeFond = NULL ;
 SDL_Rect positionFond ;

 positionFond .x = 0;
 positionFond .y = 0;


//============================================


SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (800 , 600 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Chargement d'images en SDL ", NULL );

/* Chargement d'une image Bitmap dans une surface */
imageDeFond = SDL_LoadBMP ("im/3.bmp");



 /* On blitte par - dessus l'é cran */
SDL_BlitSurface (imageDeFond , NULL , ecran , & positionFond );

SDL_Flip ( ecran );
pause ();

SDL_FreeSurface ( imageDeFond ); /* On libère la surface */





SDL_Quit ();

return EXIT_SUCCESS ;
}
 
 
 
 //cette fonction rend possible la fermeture par la croix
void pause(){



int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );
switch ( event . type )
{
case SDL_QUIT :
continuer = 0;



}
}
}
 
 
 
 /*
 
 Ici il n'y a pas d'icone de lancement integré au programme. 
 
 Remarquer  qu'à l'execution, au niveau  de la barre de tache, le fond d icone est vide. 
 
 */
 
 
/*


$ gcc -o  exe   02L01.c   -lSDL
$ ./exe

ok

$gcc -Wall -Wextra -O2 -g -o  exe   02L01.c   -lSDL
*/
 
 
 



############################################################
############################################################

/*



*/


#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>



int main (int argc , char * argv [])
{
SDL_Surface * ecran = NULL ;
SDL_Event event ; /* La variable contenant l'évé nement */
int continuer = 1; /* Notre bool éen pour la boucle */

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des évé nements en SDL", NULL );

while ( continuer ) /* TANT QUE la variable ne vaut pas 0 */
{
SDL_WaitEvent (& event ); /* On attend un évé nement qu'on
récupère dans event */
 switch ( event.type ) /* On teste le type d'évé nement */
{
case SDL_QUIT : /* Si c'est un évé nement QUITTER */
continuer = 1; /* On met le bool éen à 1, donc la boucle ne  s'arrête pas meme si on ferme la croix : cf l1 */
break ;
}
}

 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
 
 
 /*
 
 
 
 
 
 */


############################################################
############################################################


 # include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>
 
 //#include "SDL.h"      l1


//#include <sdl.h>    ou <SDL.h>  : ne voit pas 
 void pause ();

 int main (int argc , char * argv [])
{
 SDL_Init ( SDL_INIT_VIDEO ); // Initialisation de la SDL

 SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE ); // Ouverture  de la fenêtre
 
 pause (); // Mise en pause du programme

 SDL_Quit (); // Arrêt de la SDL

 return EXIT_SUCCESS ; // Fermeture du programme
 }

 void pause ()
 {
int continuer = 1;
 SDL_Event event ;

 while ( continuer )
 {
 SDL_WaitEvent (& event );
 switch ( event . type )
{
 case SDL_QUIT :
continuer = 0;
 }
}
 }
 
 
 
 
 /*
 gcc 2main.c -lSDL -o    exe
 
 
 gcc example.c -I/usr/local/include -L/usr/local/lib -lSDL

 >   
 gcc 2main.c -I/usr/local/include -L/usr/local/lib -lSDL



Toujours: compilation avec erreur de compilation ( Alw err )



3-   gcc 2main.c $(pkg-config --cflags --libs sdl2)

Alw err


=================================


gcc mon_fichier.c `sdl-config --cflags --libs`



>  :   gcc 2main.c `sdl-config --cflags --libs`


OK:  le fichier génère un executable  a.out


Et ca marche:

./a.out


 OK: parfait, la fenêtre ouvre

CONCLUSION:

Il a fallu installer  SDL2-2.0.5

Desormais, les programme de Nebra sont executable. 


 */
 
 
 
 
############################################################
############################################################
 # include <stdlib.h>
# include <stdio.h>
 # include <SDL/SDL.h>

 void pause ();

 int main (int argc , char * argv [])
{
 SDL_Init( SDL_INIT_VIDEO ); // Initialisation de la SDL

 SDL_SetVideoMode(640 , 480 , 32 , SDL_HWSURFACE ); // Ouverture de la fenêtre
 
 
 pause (); // Mise en pause du programme
 
 

 SDL_Quit (); // Arrêt de la SDL

 return EXIT_SUCCESS ; // Fermeture du programme
 }

 void pause ()
 {
int continuer = 1;
 SDL_Event event ;

 while ( continuer )
 {
 SDL_WaitEvent (& event );
 switch ( event . type )
{
 case SDL_QUIT :
continuer = 0;
 }
}
 }
 
 
 
 
 /*
 gcc 2main.c -lSDL -o    exe
 
  ./exe
 
 
 
 OK: parfait, la fenêtre ouvre
 */
 
 
 


############################################################
############################################################

/*
Associer une icône à son application
345
*/
#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

void pause();

int main ( int argc , char * argv []){

SDL_Surface * ecran = NULL , * imageDeFond = NULL ;
SDL_Rect positionFond ;

positionFond.x = 0;
positionFond.y = 0;

SDL_Init ( SDL_INIT_VIDEO );

 /* Chargement de l'icône AVANT SDL_SetVideoMode */
 
SDL_WM_SetIcon ( SDL_LoadBMP ("im/ico.bmp"), NULL );


//NB: cette icone est utilisé dans la barre de tache, le lance. C'est l'icone d'affichage  du programme. '
ecran = SDL_SetVideoMode (800 , 600 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption ("Welcome!", NULL );

imageDeFond = SDL_LoadBMP ("im/fond.bmp");
SDL_BlitSurface ( imageDeFond , NULL , ecran , & positionFond );

SDL_Flip ( ecran );


pause ();


SDL_FreeSurface ( imageDeFond );
SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
 
//----for continue 
 
 
void pause(){

int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );
switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}
}


/*

 gcc -Wall -Wextra -O2 -g -o  exe   03L01.c   -lSDL
ok
*/

############################################################
############################################################


/*   aout de l1 pour chnage le titre de la fenêtre*/




 /*
 # include <stdlib.h>
# include <stdio.h>
 # include <SDL/SDL.h>

 void pause ();

 int main (int argc , char * argv [])
{
 SDL_Init( SDL_INIT_VIDEO ); // Initialisation de la SDL

 SDL_SetVideoMode(640 , 480 , 32 , SDL_HWSURFACE ); // Ouverture de la fenêtre
 
 
 SDL_WM_SetCaption ("Ma super fenêtre SDL !", NULL );    // l1
 
 pause (); // Mise en pause du programme
 
 

 SDL_Quit (); // Arrêt de la SDL

 return EXIT_SUCCESS ; // Fermeture du programme
 }


 void pause ()
 {
int continuer = 1;
 SDL_Event event ;

 while ( continuer )
 {
 SDL_WaitEvent (& event );
 switch ( event . type )
{
 case SDL_QUIT :
continuer = 0;
 }
}
 }
 

*/
 
 
 
 /*
 gcc main.cpp -lSDL -o    exe  ; ./exe
  
 OK
 
 
 */
 
 
 

/*
==============================================================
 Gestion de surface  avec  prevention d'erreur'
*/


/*
# include <stdlib.h>
# include <stdio.h>
 # include <SDL/SDL.h>

 void pause ();


 int main (int argc , char * argv [])
 {
SDL_Surface * ecran = NULL ; // Le pointeur qui va stocker la surface de l'é cran

SDL_Init ( SDL_INIT_VIDEO );

 ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE ); //On tente d'ouvrir une fenêtre
if ( ecran == NULL ) // Si l'ouverture a é chou é, on le note et on arrête
 {
 
 //sur cette ligne qui suit : tout sur une  ligne
 
 fprintf ( stderr , " Impossible de charger le mode vidéo :%s\n", SDL_GetError ());




 exit ( EXIT_FAILURE );
 }

SDL_WM_SetCaption ("Ma super fenêtre SDL !", NULL );

pause ();

 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
 
 
 
 

 void pause ()
 {
int continuer = 1;
 SDL_Event event ;

 while ( continuer )
 {
 SDL_WaitEvent (& event );
 switch ( event . type )
{
 case SDL_QUIT :
continuer = 0;
 }
}
 }
 
*/



/*
==============================================================
 gestion des couleurs  :  l2  l3
 CREATION D'ecran'
*/

/*
 # include <stdlib.h>
# include <stdio.h>
 # include <SDL/SDL.h>

 void pause ();

 int main (int argc , char * argv [])
{

 SDL_Surface * ecran = NULL ;
 
 SDL_Init( SDL_INIT_VIDEO ); // Initialisation de la SDL


 ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
 

 
 
 SDL_WM_SetCaption ("IMHOTEP WAVES", NULL );    // l1
 
 
 // Coloration de la surface ecran en bleu - vert     l2
 
 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 17,206,112 ));

 SDL_Flip ( ecran ); // Mise à jour de l'é cran avec sa nouvelle couleur          l3   
 
 
 
 
 
 pause (); // Mise en pause du programme
 
 

 SDL_Quit (); // Arrêt de la SDL

 return EXIT_SUCCESS ; // Fermeture du programme
 }


 void pause ()
 {
int continuer = 1;
 SDL_Event event ;

 while ( continuer )
 {
 SDL_WaitEvent (& event );
 switch ( event . type )
{
 case SDL_QUIT :
continuer = 0;
 }
}
 }

*/
 
  /*
 gcc main.cpp -lSDL -o    exe  ; ./exe
  
 OK
 
 gcc main.c `sdl-config --cflags --libs`  ; ./a.out
 
 
 */
 
 
 


/*
==============================================================
 
 
 creation des surface sur l'ecran'
*/

 /*
# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>

void pause ();
 
	int main ( int argc , char * argv []){
	
	SDL_Surface * ecran = NULL , * rectangle = NULL ;
	SDL_Rect position ;
	SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

// Allocation de la surface
rectangle = SDL_CreateRGBSurface ( SDL_HWSURFACE , 220 , 180 ,
32 , 0, 0, 0, 0);

SDL_WM_SetCaption ("IMHOTEP WAVES !", NULL );

SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 17 , 206
, 112 ));

position .x = 0; // Les coordonnées de la surface seront (0,0)
position .y = 0;

// Remplissage de la surface avec du blanc

SDL_FillRect ( rectangle , NULL , SDL_MapRGB (ecran ->format , 255
, 255 , 255));


// Collage de la surface sur l'é cran
SDL_BlitSurface ( rectangle , NULL , ecran , & position ); 

 // Mise à jour de l'é cran
SDL_Flip ( ecran );

pause ();

// Libé ration de la surface
SDL_FreeSurface ( rectangle ); 

SDL_Quit ();

 return EXIT_SUCCESS ;
}

 
 
 
 
 void pause ()
 {
int continuer = 1;
 SDL_Event event ;

 while ( continuer )
 {
 SDL_WaitEvent (& event );
 switch ( event . type )
{
 case SDL_QUIT :
continuer = 0;
 }
}
 }
  


*/
 
 /*
 gcc main.cpp -lSDL -o    exe  ; ./exe
  
 OK
 
 gcc main.c `sdl-config --cflags --libs`  ; ./a.out
 
 
 */
 


/*
==============================================================
 Centrer la surface à l'écran
 351
 
 Surface centée 
*/

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>
#include <math.h>       // l1   :  avec ou sans 
void pause ();
 
int main ( int argc , char * argv []){
	
	SDL_Surface * ecran = NULL , * rectangle = NULL ;
	SDL_Rect position ;
	SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

// Allocation de la surface
rectangle = SDL_CreateRGBSurface ( SDL_HWSURFACE , 220 , 180 ,
32 , 0, 0, 0, 0);

SDL_WM_SetCaption ("IMHOTEP WAVES !", NULL );

SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 17 , 206
, 112 ));


// avec : moitié de la largeur de l'écran (640 / 2)

//et moitié de la largeur du rectangle (220 / 2)
  position .x = ( 640 / 2) - ( 220 / 2); 
  position .y = ( 480 / 2) - ( 180 / 2);

// Remplissage de la surface avec du blanc

SDL_FillRect ( rectangle , NULL , SDL_MapRGB (ecran ->format , 255
, 255 , 255));


// Collage de la surface sur l'é cran
SDL_BlitSurface ( rectangle , NULL , ecran , & position ); 

 // Mise à jour de l'é cran
SDL_Flip ( ecran );

pause ();

// Libé ration de la surface
SDL_FreeSurface ( rectangle ); 

SDL_Quit ();

 return EXIT_SUCCESS ;
}

 
 
 
 
 void pause ()
 {
int continuer = 1;
//---------utilisation de SDL_Event event --------

//NOTA  : SDL_Event est une structure definie dans 

//SDL_events.h   , l 227

//mise en facteur de la variable event  definie dans la fct :

// extern DECLSPEC int SDLCALL SDL_WaitEvent(SDL_Event *event);   , l 290
                   
 
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );	//cf fct SDL_WaitEvent(SDL_Event *event);    ,l 290     

switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}


//NB: SDL_events.h est inclus dans SDL.h .
//cf: SDL.h :  l37 , #include "SDL_events.h"
}




 
 /*
gcc 3main.cpp -lSDL -o    exe  ; ./exe
  

OK
 
gcc main.c `sdl-config --cflags --libs`  ; ./a.out


------------------------------

Sur Codeblocks :  ok

via gcc : il demande quelque chose
 
*/
 
 
 


############################################################
############################################################
/* */
/*

Ajouter une image sur une autre
On va blitter l'image de Zozor 3 sur la scène :
*/

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

void pause();


 int main (int argc , char * argv []) {
 
 SDL_Surface * ecran = NULL , * imageDeFond = NULL , * zozor = NULL ;
SDL_Rect positionFond , positionZozor ;

positionFond.x = 0;
positionFond.y = 0;
positionZozor.x = 50;
positionZozor.y = 26;

SDL_Init ( SDL_INIT_VIDEO );
SDL_WM_SetIcon ( SDL_LoadBMP ("im/ico.bmp"), NULL );
ecran = SDL_SetVideoMode (800 , 600 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption ("HI !!!", NULL );
imageDeFond = SDL_LoadBMP ("im/f1.bmp");
SDL_BlitSurface ( imageDeFond , NULL , ecran , &positionFond);

 /* Chargement et blittage de Zozor sur la scène */
 zozor = SDL_LoadBMP ("im/5.bmp");
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );

 SDL_Flip (ecran);
 pause ();

 SDL_FreeSurface ( imageDeFond );
 SDL_FreeSurface ( zozor );
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }




void pause(){

int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );
switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}
}

/*
$ gcc -Wall -Wextra -O2 -g -o  exe   04L01.c   -lSDL

OK

*/


############################################################
############################################################



/*
==============================================================
 Centrer la surface à l'écran
 351
 
 Surface centée 
*/

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>
#include <math.h>       // l1   :  avec ou sans 
void pause ();
 
int main ( int argc , char * argv []){
	
	SDL_Surface * ecran = NULL , * rectangle = NULL ;
	SDL_Rect position ;
	SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

// Allocation de la surface
rectangle = SDL_CreateRGBSurface ( SDL_HWSURFACE , 220 , 180 ,
32 , 0, 0, 0, 0);

SDL_WM_SetCaption ("IMHOTEP WAVES !", NULL );

SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 17 , 206
, 112 ));


// avec : moitié de la largeur de l'écran (640 / 2)

//et moitié de la largeur du rectangle (220 / 2)
  position .x = ( 640 / 2) - ( 220 / 2); 
  position .y = ( 480 / 2) - ( 180 / 2);

// Remplissage de la surface avec du blanc

SDL_FillRect ( rectangle , NULL , SDL_MapRGB (ecran ->format , 255
, 255 , 255));


// Collage de la surface sur l'é cran
SDL_BlitSurface ( rectangle , NULL , ecran , & position ); 






 // Mise à jour de l'é cran
SDL_Flip ( ecran );





pause ();

// Libé ration de la surface
SDL_FreeSurface ( rectangle ); 

SDL_Quit ();

 return EXIT_SUCCESS ;
}

 
 
 
 
 void pause ()
 {
int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );	//cf fct SDL_WaitEvent(SDL_Event *event);    ,l 290     

 switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}

}



/*

gcc -o  exe   01-1L04.c   -lSDL



 gcc 01-1L04.c   -lSDL -o    exe  ; ./exe
 
 
gcc -o  exe   01-1L04.c   -lSDL 
 

*/

############################################################
############################################################




/*
==============================================================
 Centrer la surface à l'écran
 351
 
 Surface centée 
*/

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>
#include <math.h>       // l1   :  avec ou sans 



void echo ();
void pause ();
 
int main ( int argc , char * argv []){
	
	SDL_Surface * ecran = NULL , * rectangle = NULL ;
	SDL_Rect position ;
	SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

// Allocation de la surface
rectangle = SDL_CreateRGBSurface ( SDL_HWSURFACE , 220 , 180 ,
32 , 0, 0, 0, 0);

SDL_WM_SetCaption ("IMHOTEP WAVES !", NULL );

SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 17 , 206
, 112 ));


// avec : moitié de la largeur de l'écran (640 / 2)

//et moitié de la largeur du rectangle (220 / 2)
  position .x = ( 640 / 2) - ( 220 / 2); 
  position .y = ( 480 / 2) - ( 180 / 2);

// Remplissage de la surface avec du blanc

SDL_FillRect ( rectangle , NULL ,     );


// Collage de la surface sur l'é cran
SDL_BlitSurface ( rectangle , NULL , ecran , & position ); 






 // Mise à jour de l'é cran
SDL_Flip ( ecran );





pause ();

// Libé ration de la surface
SDL_FreeSurface ( rectangle ); 

SDL_Quit ();

 return EXIT_SUCCESS ;
}

 
 
 
 
 void pause (){
int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );	//cf fct SDL_WaitEvent(SDL_Event *event);    ,l 290     

 switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}

}

 void echo (){
 
 
 printf("bonjour");
 }
 
 
/*

gcc -Wall -Wextra -O2 -g -o exe   01-2L04.c   -lSDL



 gcc 01-1L04.c   -lSDL -o    exe  ; ./exe
 


*/


############################################################
###########################################################




/* */
/*

Gestion de la transparence

Le problème de la transparence

Nous avons tout à l'heure chargé une image bitmap dans notre fenêtre. Supposons que l'on veuille blitter une image par-dessus. Ça vous arrivera très fréquemment car dans un jeu, en général, le personnage que l'on déplace est un Bitmap et il se déplace sur

une image de fond.
*/


#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>


void pause();


 int main (int argc , char * argv []) {
 
 SDL_Surface * ecran = NULL , * imageDeFond = NULL , * zozor = NULL ;
SDL_Rect positionFond , positionZozor ;

positionFond.x = 0;
positionFond.y = 0;
positionZozor.x = 345;
positionZozor.y = 260;


SDL_Init ( SDL_INIT_VIDEO );
SDL_WM_SetIcon ( SDL_LoadBMP ("im/ico.bmp"), NULL );
ecran = SDL_SetVideoMode (800 , 600 , 32 , SDL_HWSURFACE );


//----titre-------------
SDL_WM_SetCaption ("HI !!!", NULL );

//----------------------

imageDeFond = SDL_LoadBMP ("im/f1.bmp");
SDL_BlitSurface ( imageDeFond , NULL , ecran , &positionFond);

 /* Chargement et blittage de Zozor sur la scène */
 zozor = SDL_LoadBMP ("im/6.bmp");
 
 
// Pour la transparence
 
 
//SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor ->format,255,255,255 ));
 
 //pour la Transparence Alpha moyenne (128 ) : 
 
 //SDL_SetAlpha (zozor , SDL_SRCALPHA , 128 );
 
 //blittage
 
 
 
 
SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 
 
 
 SDL_Flip (ecran);
 pause ();

 SDL_FreeSurface ( imageDeFond );
 SDL_FreeSurface ( zozor );
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }




void pause(){

int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );
switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}
}

/*
$ gcc -Wall -Wextra -O2 -g -o  exe   04L01.c   -lSDL

OK

*/

############################################################
############################################################


/* j'inclue SDL/SDL_image.h et que je ne fais pas appel à SDL_SetColorKey car mon
PNG est naturellement transparent. Vous allez voir que j'utilise IMG_Load partout dans
ce code en remplacement de SDL_LoadBMP.



_ Comme SDL_image peut aussi ouvrir les BMP, vous pouvez même oublier
maintenant la fonction SDL_LoadBMP et ne plus utiliser que IMG_Load pour
le chargement de n'importe quelle image.

*/

#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

//#include <SDL_image.h>   : non, car SDL_image .h est dans SDL  : cf usr/include




void pause();

int main ( int argc , char * argv []){

SDL_Surface * ecran = NULL , * imageDeFond = NULL , * sapin =NULL ;
 
SDL_Rect positionFond , positionSapin ;

positionFond.x = 0;
positionFond.y = 0;
positionSapin.x = 345;
positionSapin.y = 260;


/* [...] Chargement de la SDL et d'une image de fond */





SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (800 , 600 , 32 , SDL_HWSURFACE );

//----titre-------------


SDL_WM_SetCaption ("linux is not unix !", NULL );


// image de fond  par : IMG_Load 
 
 
//imageDeFond =IMG_Load ("im/f1.bmp");

//imageDeFond = SDL_LoadBMP ("im/f1.bmp");
//SDL_BlitSurface ( imageDeFond , NULL , ecran , &positionFond);

/* Chargement d'un PNG avec IMG_Load
Celui -ci est automatiquement rendu transparent car les
informations de transparence sont codées à l'int é rieur du fichier PNG */



sapin = SDL_LoadBMP ("im/6.bmp");

//sapin = IMG_Load ("im/sapin.png");
SDL_BlitSurface(sapin , NULL , ecran , & positionSapin );



SDL_Flip ( ecran );

pause ();

SDL_FreeSurface ( imageDeFond );
SDL_FreeSurface ( sapin );
SDL_Quit ();

return EXIT_SUCCESS ;
}
 


void pause(){

int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );
switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}
}
 
 

############################################################
############################################################


#include <stdlib .h>
#include <stdio .h>
#include <SDL/SDL.h>
#include <SDL/ SDL_image .h>

 /* Inclusion du header de SDL_image
( adapter le dossier au besoin ) */

int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * imageDeFond = NULL , * sapin =
NULL ;

SDL_Rect positionFond , positionSapin ;

/* [...] Chargement de la SDL et d'une image de fond */

/* Chargement d'un PNG avec IMG_Load
Celui -ci est automatiquement rendu transparent car les
informations de
transparence sont codées à l'int é rieur du fichier PNG */
sapin = IMG_Load (" sapin .png ");
SDL_BlitSurface (sapin , NULL , ecran , & positionSapin );

SDL_Flip ( ecran );



pause ();
SDL_FreeSurface ( imageDeFond );
SDL_FreeSurface ( sapin );
SDL_Quit ();

return EXIT_SUCCESS ;
}



############################################################
############################################################




/* */
/*

une image de fond.
*/


#include <stdlib.h>
#include <stdio.h>
//#include <SDL/SDL_image.h>

#include "kchrSDL_image.h"

//kchrSDL_image.h

void pause();


 int main (int argc , char * argv []) {
 
 SDL_Surface * ecran = NULL , * imageDeFond = NULL , * sapin = NULL ;
SDL_Rect positionFond , positionSapin ;

//* sapin = NULL ;


positionFond.x = 0;
positionFond.y = 0;
positionSapin.x = 345;
positionSapin.y = 260;








SDL_Init ( SDL_INIT_VIDEO );
SDL_WM_SetIcon ( SDL_LoadBMP ("im/ico.bmp"), NULL );
ecran = SDL_SetVideoMode (800 , 600 , 32 , SDL_HWSURFACE );


//----titre-------------
SDL_WM_SetCaption ("linux is not unix ", NULL );

//----------------------

imageDeFond = SDL_LoadBMP ("im/f1.bmp");
SDL_BlitSurface ( imageDeFond , NULL , ecran , &positionFond);

 /* Chargement et blittage de Zozor sur la scène */
 //zozor = SDL_LoadBMP ("im/6.bmp");
 
//sapin = SDL_LoadBMP ("im/sapin.png");
 

 
//sapin = IMG_Load("im/sapin.png");
//sapin =IMG_Load("im/sapin.png");
 
 
  
// Pour la transparence

 
 
//SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor ->format,255,255,255 ));
 
 //pour la Transparence Alpha moyenne (128 ) : 
 
 //SDL_SetAlpha (zozor , SDL_SRCALPHA , 128 );
 
 //blittage
 
 SDL_BlitSurface (sapin , NULL , ecran , &positionSapin);
 
 
 
 SDL_Flip (ecran);
 pause ();

 SDL_FreeSurface ( imageDeFond );
 SDL_FreeSurface (sapin);
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
 
 
 
 




void pause(){

int continuer = 1;
SDL_Event event ;
while ( continuer )
{
SDL_WaitEvent (& event );
switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}
}

/*
$ gcc -Wall -Wextra -O2 -g -o  exe   04L01.c   -lSDL


collect2: error: ld returned 1 exit status


see  : lien de compilation avec SDL_image.h

*/

/*





*/




############################################################
############################################################


/*
==============================================================
Fermer un programme en appuyant la croix  :   ou sur une touche du clavier


*/

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>
#include <math.h>       // l1   :  avec ou sans 



void pause ();
 
int main ( int argc , char * argv []){
	
	SDL_Surface * ecran = NULL , * rectangle = NULL ;
	SDL_Rect position ;
	SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

// Allocation de la surface
rectangle = SDL_CreateRGBSurface ( SDL_HWSURFACE , 220 , 180 ,
32 , 0, 0, 0, 0);

SDL_WM_SetCaption ("IMHOTEP WAVES !", NULL );

SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 17 , 206
, 112 ));


// avec : moitié de la largeur de l'écran (640 / 2)

//et moitié de la largeur du rectangle (220 / 2)
  position .x = ( 640 / 2) - ( 220 / 2); 
  position .y = ( 480 / 2) - ( 180 / 2);

// Remplissage de la surface avec du blanc

SDL_FillRect ( rectangle , NULL , SDL_MapRGB (ecran ->format , 255
, 255 , 255));


// Collage de la surface sur l'é cran
SDL_BlitSurface ( rectangle , NULL , ecran , & position ); 


printf("bonjour");  // pas d'effet maintenant'



 // Mise à jour de l'é cran
SDL_Flip ( ecran );




pause ();

// Libé ration de la surface
SDL_FreeSurface ( rectangle ); 

SDL_Quit ();

 return EXIT_SUCCESS ;
}

 
 
 
void pause (){
 
 
int continuer = 1;
SDL_Event event ;


while ( continuer ){
SDL_WaitEvent (&event );
switch(event .type ){
case SDL_QUIT :
 continuer = 0;
break ;
case SDL_KEYDOWN : /* Si appui sur une touche */
continuer = 0;
break ;
}

}


}
 



//=============================================

/*
gcc -o  exe   08L01.c   -lSDL   -lm


ok

 ./exe

En appyant sur une touche, ou en fermant la croix  : le programme se  ferme

*/
 
/*
 
 Ancien contenu de pause
 
 void pause ()
 {
 
 
int continuer = 1;
SDL_Event event ;



while ( continuer )
{
SDL_WaitEvent (& event );
 switch ( event . type )
{
case SDL_QUIT :
continuer = 0;
}
}

}

*/


/*
	//cf fct SDL_WaitEvent(SDL_Event *event);    ,l 290   
	
	  

gcc -o  exe   01-1L04.c   -lSDL



 gcc 01-1L04.c   -lSDL -o    exe  ; ./exe
 
 
gcc -o  exe   01-1L04.c   -lSDL 
 

*/


############################################################
############################################################
/*
==============================================================
Fermer un programme en appuyant la croix  :   ou sur la touche du echap


NB: sur Dell
: echap  : 2er ligne du clavier , position 1



*/


# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>
 
int main ( int argc , char * argv []){


int continuer =1 ; //  booléen for  boucle *
SDL_Event event ;


//=====================================
	SDL_Surface * ecran = NULL , * rectangle = NULL ;
	SDL_Rect position ;
	
	SDL_Init ( SDL_INIT_VIDEO );
	
ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );

// Allocation de la surface
rectangle = SDL_CreateRGBSurface ( SDL_HWSURFACE , 220 , 180 ,32 , 0, 0, 0, 0);

SDL_WM_SetCaption ("IMHOTEP WAVES !", NULL );

SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 17 , 206, 112 ));


// avec : moitié de la largeur de l'écran (640 / 2)

//et moitié de la largeur du rectangle (220 / 2)
  position .x = ( 640 / 2) - ( 220 / 2); 
  position .y = ( 480 / 2) - ( 180 / 2);




//======================================================




//------noter bien la place de ce code-----------------------------

while (continuer){



	SDL_WaitEvent(&event);
	switch (event.type){
	case SDL_QUIT:
		continuer = 0;
		break ;
	case SDL_KEYDOWN:
		switch (event.key.keysym.sym){
			case SDLK_ESCAPE: 

			/*Appui sur la touche Echap,
			on arrête le programme */
			continuer = 0;
			break ;
		}
		break ;
	}
	
//_________ATTENTE DE  : SDL_FillRect, SDL_BlitSurface , SDL_Flip  : C'EST CE QUI SERA PERENISE PAR continuer

// Remplissage de la surface avec du blanc

SDL_FillRect ( rectangle , NULL , SDL_MapRGB (ecran ->format , 255
, 255 , 255));


// Collage de la surface sur l'é cran
SDL_BlitSurface ( rectangle , NULL , ecran , & position ); 

 // Mise à jour de l'écran
SDL_Flip ( ecran );	
	
	
	
	
	
	
	
}

//----------------------------------







//****** Libération de la surface :  AVANT LE RETURN

SDL_FreeSurface (rectangle); 

SDL_Quit ();

//****** *************************



return EXIT_SUCCESS ;
}


/*

$ gcc -o  exe   09L01.c   -lSDL  
$ ./exe


OK
*/
 


############################################################
#############################################################


/*
EXERCICE : DIRIGER zozor AU CLAVIER

*/

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );
switch ( event . type ){
case SDL_QUIT :
continuer = 0;
break ;
case SDL_KEYDOWN :
switch ( event.key.keysym.sym){
case SDLK_UP : // Flè che haut
positionZozor.y --;





break ;
case SDLK_DOWN : // Flèche bas
positionZozor.y ++;
break ;
case SDLK_RIGHT : // Flèche droite
positionZozor.x ++;
break ; 
case SDLK_LEFT : // Flèche gauche
positionZozor.x --;
break ;
}
break ;
}

 /* On efface l'é cran */
 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }
 
 
 

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
  gcc -o  exe   10L01.c   -lSDL 
  
  OK


*/


/*

NOTA:  sur:
Le centrage


 positionZozor .x = ecran ->w / 2 - zozor ->w / 2;
 positionZozor .y = ecran ->h / 2 - zozor ->h / 2;


Vous devez initialiser positionZozor après avoir chargé les surfaces ecran
et zozor. En effet, j'utilise la largeur (w  , wide  ) et la hauteur (h  , height) de ces deux surfaces pour calculer la position centrée de Zozor à l'écran, il faut donc que  ces surfaces aient été initialisées auparavant.



*/




###########################################################
############################################################
/*
Avec

deplacement d'icone sur  -5px  et  +5px  */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );
switch ( event . type ){
case SDL_QUIT :
continuer = 0;
break ;
case SDL_KEYDOWN :
switch ( event.key.keysym.sym){
case SDLK_UP : // Flè che haut
positionZozor.y --;
break ;
case SDLK_DOWN : // Flèche bas
positionZozor.y ++;
break ;
case SDLK_RIGHT : // Flèche droite
//positionZozor.x ++;

// deplacement de cinq +5px
positionZozor.x = positionZozor.x +5;
break ; 
case SDLK_LEFT : // Flèche gauche
//positionZozor.x --;

// deplacement de cinq -5px
positionZozor.x = positionZozor.x -5;

break ;
}
break ;
}


 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }
 
 
 

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
  gcc -o  exe   10L01.c   -lSDL 
  
  OK


*/



############################################################
############################################################

/*
Avec  SDL_EnableKeyRepeat

deplacement d'icone sur  -5px  et  +5px  */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){



//================================
SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );



switch ( event . type ){

case SDL_QUIT :
continuer = 0;
SDL_EnableKeyRepeat (10 ,10);
break ;


case SDL_KEYDOWN :



switch ( event.key.keysym.sym){



		
	case SDLK_UP : 

		positionZozor.y --;

		SDL_EnableKeyRepeat (10 ,10);  //l1
		
		
	break ;

	case SDLK_DOWN : // Flèche bas
		
		positionZozor.y ++;
		SDL_EnableKeyRepeat (10 ,10);  //l2
		
		

	break ;
	
	case SDLK_RIGHT : // Flèche droite
		

		// deplacement de cinq +5px
		
		positionZozor.x = positionZozor.x +5;
		
		//SDL_EnableKeyRepeat (10 ,10);  //l3
		
	break ; 
	case SDLK_LEFT : // Flèche gauche

		// deplacement de cinq -5px
		positionZozor.x = positionZozor.x -5;
		

	break ;
}


 
break ;
}

 /* On efface l'é cran */
 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }
 
 
 

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
gcc -Wall -Wextra -O2 -g -o exe  12L01.c   -lSDL 
  
  OK


l1  et l2  : influence SDLK_LEFT  et SDLK_RIGHT




*/




############################################################
############################################################


/* allow for key repeat (so the user can hold down a key...) 
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

	*/
	
	
	



	
	
	

/*
Avec  SDL_EnableKeyRepeat

deplacement d'icone sur  -5px  et  +5px  */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){



//================================
SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'écran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;


	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);  //l5

while ( continuer ){
SDL_WaitEvent (& event );



switch ( event . type ){

case SDL_QUIT :
continuer = 0;

break ;


case SDL_KEYDOWN :
switch ( event.key.keysym.sym){



		
	case SDLK_UP : 

		positionZozor.y = positionZozor.y - 15;
		//SDL_EnableKeyRepeat (10 ,10);  //l1
		
		
	break ;

	case SDLK_DOWN : // Flèche bas
		
		positionZozor.y = positionZozor.y + 15;

		//SDL_EnableKeyRepeat (10 ,10);  //l2
		
		
	break ;


	case SDLK_RIGHT :

		
		positionZozor.x = positionZozor.x +5;
		//SDL_EnableKeyRepeat (10 ,10);  //l3
		
		
	break ; 
	case SDLK_LEFT : // Flèche gauche

		
		positionZozor.x = positionZozor.x -5;
		//SDL_EnableKeyRepeat (10 ,10);  //l4

	break ;
}


 
break ;
}

 /* On efface l'é cran */
 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }
 
 
 

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
 
 
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
gcc -Wall -Wextra -O2 -g -o exe  13-1L01.c   -lSDL 
  
  OK
  
  ligne : l5 
  
  inspiré de /home/vieira/Bureau/C/Computer-Science/LEARNING/PROGRAMMING/FUND-PROG/C/ALL-C-PROG/FROM-CODES-SOURCES/sdl-demo/kchr-test/latin1.c
  
gcc -o exe  13-1L01.c   -lSDL 

bien: parfait 


 
*/





############################################################
############################################################

/*PARFAIT

//PERFECTION DE SDL_EnableKeyRepeat : FAIRE APPEL DE CETTE FONCTION JUSTE AVANT L'ENTREE DE DANS LA BOUCLE


*/

/* allow for key repeat (so the user can hold down a key...) 
	SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
	
	
	
	*/
	
	
	

/*
Avec  SDL_EnableKeyRepeat

deplacement d'icone sur  -5px  et  +5px  */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>

int main ( int argc , char * argv []){



//================================
SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'écran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

//PERFECTION DE SDL_EnableKeyRepeat : FAIRE APPEL DE CETTE FONCTION JUSTE AVANT L'ENTREE DE DANS LA BOUCLE
	SDL_EnableKeyRepeat(10, 1);  //l5

while ( continuer ){
SDL_WaitEvent (& event );



switch ( event . type ){

case SDL_QUIT :
continuer = 0;

break ;


case SDL_KEYDOWN :
switch ( event.key.keysym.sym){

	case SDLK_UP : 

		positionZozor.y = positionZozor.y - 15;

		break ;

	case SDLK_DOWN : // Flèche bas

		positionZozor.y = positionZozor.y + 15;
		
		break ;

	case SDLK_RIGHT :
		positionZozor.x = positionZozor.x +5;
		break ; 
		
	case SDLK_LEFT : // Flèche gauche
	
		positionZozor.x = positionZozor.x -5;


		break ;
}


 
break ;
}

 /* On efface l'é cran */
 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }
 
 
 

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
/*

./exe

  OK

gcc -o exe  13-2L01.c   -lSDL 

//PERFECTION DE SDL_EnableKeyRepeat : FAIRE APPEL DE CETTE FONCTION JUSTE AVANT L'ENTREE DE DANS LA BOUCLE

bien: parfait 

SDL_EnableKeyRepeat(10, 10);


SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);
SDL_EnableKeyRepeat(1, 10);  : vite
SDL_EnableKeyRepeat(10, 1);  plus vite 


*/





############################################################
############################################################


/*
Avec  SDL_EnableKeyRepeat

deplacement d'icone sur  -5px  et  +5px  */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){



//================================
SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (640 , 480 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'écran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );
switch ( event . type ){

case SDL_QUIT :
continuer = 0;

break ;


case SDL_KEYDOWN :
switch ( event.key.keysym.sym){
	case SDLK_UP : 

		positionZozor.y = positionZozor.y - 15;
		//SDL_EnableKeyRepeat (10 ,10);  //l1
		
		
	break ;

	case SDLK_DOWN : // Flèche bas
		
		positionZozor.y = positionZozor.y + 15;

		//SDL_EnableKeyRepeat (10 ,10);  //l2
		
		
	break ;


	case SDLK_RIGHT :

		
		positionZozor.x = positionZozor.x +5;
		SDL_EnableKeyRepeat (10 ,10);  //l3
		
		
	break ; 
	case SDLK_LEFT : // Flèche gauche

		
		positionZozor.x = positionZozor.x -5;
		SDL_EnableKeyRepeat (10 ,10);  //l4

	break ;
}


 
break ;
}

 /* On efface l'é cran */
 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }
 
 
 

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
 }
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
gcc -Wall -Wextra -O2 -g -o exe  12L01.c   -lSDL 
  
  OK


*/


/*
l1  et l2  : influence SDLK_LEFT  et SDLK_RIGHT


l3  et l4 : influence SDLK_DOWN  et SDLK_UP


Utilser seulement  1 couple à la fois: pas les 2.

ie : l1  et l2  : influence SDLK_LEFT  et SDLK_RIGHT


ou bien 

l3  et l4 : influence SDLK_DOWN  et SDLK_UP


ETNON LES DEUX A LA FOIS



*/


############################################################
############################################################



/*
 Parfait: 2 facon de fermer le  programme: clic droit  et croix */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

//double buffering
ecran = SDL_SetVideoMode ( 640 , 480 , 32 , SDL_HWSURFACE | SDL_DOUBLEBUF ) ;

SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );  // s'il manque, event.type : ne lance pas
switch (event.type ){
	case SDL_QUIT:
		continuer = 0;
		break ;
	case SDL_MOUSEBUTTONUP:
		if (event.button.button == SDL_BUTTON_RIGHT ) /* On 	arrê
	te le programme si on a fait un clic droit */
 		continuer = 0;
		break ;

}




 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
gcc -o  exe   14L01.c   -lSDL 
  
  OK   Parfait: 2 facon de fermer le  programme: clic droit  et croix


*/



#######################################################
#######################################################





/*
 Parfait: 2 facon de fermer le  programme: clic droit  et croix */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

//double buffering
ecran = SDL_SetVideoMode ( 640 , 480 , 32 , SDL_HWSURFACE | SDL_DOUBLEBUF ) ;

SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );  // s'il manque, event.type : ne lance pas
switch (event.type ){
	case SDL_QUIT:
		continuer = 0;
		break ;
	case SDL_MOUSEBUTTONUP:
		if (event.button.button == SDL_BUTTON_RIGHT ) /* On 	arrê
	te le programme si on a fait un clic droit */
 		continuer = 0;
		break ;

}




 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
 SDL_Flip ( ecran );
 }

 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
gcc -o  exe   14L01.c   -lSDL 
  
  OK   Parfait: 2 facon de fermer le  programme: clic droit  et croix


*/



############################################################
############################################################





/*
Récupérer les coordonnées de la souris


on va blitter Zozor à l'endroit du clic de la souris. 


Voilà une information très intéressante : les coordonnées de la souris au moment du clic ! On les récupère à l'aide de deux variables (pour l'abscisse et l'ordonnée) :
event.button.x et event.button.y.
  */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

//double buffering
ecran = SDL_SetVideoMode ( 640 , 480 , 32 , SDL_HWSURFACE | SDL_DOUBLEBUF ) ;

SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );

	switch(event.type ){
		case SDL_QUIT :
			continuer = 0;
			break ;
		case SDL_MOUSEBUTTONUP :
			positionZozor.x = event.button.x;
			positionZozor .y = event.button.y;
			break ;
		}

 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 ,255 , 255));
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor ); /* On
place Zozor à sa nouvelle position */
 SDL_Flip ( ecran );
 }






 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
gcc -o  exe   15L01.c   -lSDL 
  
  OK   l'image  suit le clic de la souris 

*/





############################################################
############################################################

/*
Gérer le déplacement de la souris

On va placer Zozor aux mêmes coordonnées que la souris,

*/


# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

//double buffering
ecran = SDL_SetVideoMode ( 640 , 480 , 32 , SDL_HWSURFACE | SDL_DOUBLEBUF ) ;

SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;

while ( continuer ){
SDL_WaitEvent (& event );
switch ( event.type ){
	case SDL_QUIT :
		continuer = 0;
		 break ;
	case SDL_MOUSEMOTION :
 		positionZozor.x = event.motion.x;
 		positionZozor.y = event.motion.y;
 		break ;
}
 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 ,255 , 255));
SDL_BlitSurface (zozor , NULL , ecran , & positionZozor ); /* On
place Zozor à sa nouvelle position */
 SDL_Flip ( ecran );
}


 
SDL_FreeSurface (zozor);
SDL_Quit ();

 return EXIT_SUCCESS ;
}



/*
 gcc -o  exe   16L01.c   -lSDL 

l'image  est collé au curseurs de la souris

ok


*/




############################################################
############################################################


/*
 17L01.c  
*/


/*


Masquer la souris


 fonction  

SDL_ShowCursor et de lui envoyer un fl ag :
_ SDL_DISABLE : masque le curseur de la souris ;
_ SDL_ENABLE : réa_che le curseur de la souris.*/

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

//double buffering
ecran = SDL_SetVideoMode ( 640 , 480 , 32 , SDL_HWSURFACE | SDL_DOUBLEBUF ) ;

SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;


SDL_ShowCursor ( SDL_DISABLE );
while ( continuer ){
SDL_WaitEvent (& event );

	switch(event.type ){
		case SDL_QUIT :
			continuer = 0;
			break ;
		case SDL_MOUSEBUTTONUP :
			positionZozor.x = event.button.x;
			positionZozor .y = event.button.y;
			break ;
		}

 SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 ,255 , 255));
 SDL_BlitSurface (zozor , NULL , ecran , & positionZozor ); /* On
place Zozor à sa nouvelle position */
 SDL_Flip ( ecran );
 }






 SDL_FreeSurface (zozor);
 SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
 gcc -o  exe   17L01.c   -lSDL 

  OK   
  
  
Le curseur de la souris est masqué. 
  
*/




############################################################
############################################################

/*
 18L01.c  
*/


/*Les événements de la fenêtre

un événement de type SDL_VIDEORESIZE est généré.
-faire en sorte que notre Zozor soit toujours centré dans la fenêtre :


*/

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>



int main ( int argc , char * argv []){


SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

//ajouter le flag SDL_RESIZABLE dans la fonction   SDL_SetVideoMode :

ecran = SDL_SetVideoMode ( 640 , 480 , 32 , SDL_HWSURFACE | SDL_DOUBLEBUF  | SDL_RESIZABLE  ) ;

SDL_WM_SetCaption (" Gestion des événements en SDL", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/ico1.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'é cran */
/*positionZozor.x = ecran ->w / 2 - zozor ->w / 2;*/
/*positionZozor.y = ecran ->h / 2 - zozor ->h / 2;*/


SDL_ShowCursor ( SDL_DISABLE );
while ( continuer ){
SDL_WaitEvent (& event );

	switch(event.type ){
		case SDL_QUIT :
			continuer = 0;
			break ;
		 case SDL_VIDEORESIZE:
			positionZozor.x = event.resize.w / 2 - zozor ->w / 2;
			positionZozor.y = event.resize.h / 2 - zozor ->h / 2;
			break 
		}

SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 ,255 , 255));
SDL_BlitSurface (zozor , NULL , ecran , & positionZozor ); /* On
place Zozor à sa nouvelle position */
SDL_Flip ( ecran );
}

SDL_FreeSurface (zozor);
SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
/*

./exe
Erreur de segmentation   :  i.e :  l'image chargé n'est pas visible.'


Cf : l1


//================================
gcc -o  exe   18L01.c   -lSDL 

  OK   
*/




############################################################
############################################################

/*
Avec  SDL_EnableKeyRepeat

deplacement d'icone sur  -5px  et  +5px  */

# include <stdlib.h>
# include <stdio.h>
# include <SDL/SDL.h>
int main ( int argc , char * argv []){
//================================
SDL_Surface * ecran = NULL , * zozor = NULL ;
SDL_Rect positionZozor ;
SDL_Event event ;
int continuer = 1;

SDL_Init ( SDL_INIT_VIDEO );

ecran = SDL_SetVideoMode (720 , 530 , 32 , SDL_HWSURFACE );
SDL_WM_SetCaption ("I_Lab-SAT", NULL );

/* Chargement de Zozor */
zozor = SDL_LoadBMP ("im/sat.bmp");   //l1
SDL_SetColorKey (zozor , SDL_SRCCOLORKEY , SDL_MapRGB (zozor -> format , 0, 0, 255));

/* On centre Zozor à l'écran */
positionZozor.x = ecran ->w / 2 - zozor ->w / 2;
positionZozor.y = ecran ->h / 2 - zozor ->h / 2;


SDL_EnableKeyRepeat (10 ,10); 

while ( continuer ){
SDL_WaitEvent (& event );



switch ( event . type ){

case SDL_QUIT :
continuer = 0;

break ;


case SDL_KEYDOWN :
switch ( event.key.keysym.sym){

	case SDLK_UP : 
		positionZozor.y = positionZozor.y - 15;
		
	break ;

	case SDLK_DOWN :
		
		positionZozor.y = positionZozor.y + 15;

		
	break ;


	case SDLK_RIGHT :

		positionZozor.x = positionZozor.x +5;

		
	break ; 
	case SDLK_LEFT : 

		positionZozor.x = positionZozor.x -5;

	break ;
}


 
break ;
}

 /* On efface l'é cran */
SDL_FillRect (ecran , NULL , SDL_MapRGB (ecran -> format , 255 , 255 , 255));
/* On place Zozor à sa nouvelle position */
SDL_BlitSurface (zozor , NULL , ecran , & positionZozor );
 /* On met à jour l'affichage */
SDL_Flip ( ecran );
}
 
 
 

SDL_FreeSurface (zozor);
SDL_Quit ();

 return EXIT_SUCCESS ;
}
 
 
/*


gcc -o exe  1.c   -lSDL 
  
  OK




*/


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
#######################################################################






#################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
#######################################################################





#################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
#######################################################################








#################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
#########################################################




###############################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
#######################################################################





#################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
#######################################################################






#################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
###################################################################

####




#################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
########################################################




################################################################
############################################################


############################################################
############################################################




############################################################
############################################################



############################################################
############################################################



############################################################
############################################################



############################################################
############################################################
